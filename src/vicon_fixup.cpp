/* vicon fixup node
   Translates vicon coordinates from their convention to ROS std NED convention
   Accepts TransformStamped
   Produces:
   navmsg::Odometry
  TransformStamped

*/

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <pthread.h>

using namespace std;
using namespace ros;
pthread_mutex_t topicBell;
pthread_cond_t newPose;

geometry_msgs::TransformStampedConstPtr lastPose;


void odomCallback( geometry_msgs::TransformStampedConstPtr tf)
{
  lastPose = tf;
  pthread_cond_signal(&newPose);
}

int main(int argc, char **argv)
{

  init(argc, argv, "vicon_fixup_node");

  NodeHandle nh("~");
  ros::AsyncSpinner spinner(2);
  spinner.start();
  Publisher pose_pub = nh.advertise<geometry_msgs::PoseStamped>("pose", 1000);
  Publisher odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 1000);

  string src_topic_;
  nh.param<std::string>("src_odom", src_topic_, "/odom_in");
  ROS_INFO("Listening on [%s]", src_topic_.c_str());
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  Subscriber odom_sub = nh.subscribe(src_topic_, 1000, odomCallback);
  while (ros::ok())
  {
    //Only publish when there's at pose to share
    //Return false if we timed out in the cond_wait
    struct timespec timeout;
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_sec +=1;

    int ret;
    ret = pthread_cond_timedwait(&newPose, &topicBell, &timeout);
    if (ret == ETIMEDOUT)
      {
	ROS_INFO("No Vicon poses received!");
	continue;
      }
    
    //Munge the source transform into the output data products and publish
    
    float vicon_x = lastPose->transform.translation.x;
    float vicon_y = lastPose->transform.translation.y;
    float vicon_z = lastPose->transform.translation.z;
    float mocap_N = -1*vicon_z;
    float mocap_W = vicon_x;
    float mocap_U = -1*vicon_y;
    
    //First, the PoseStamped:
    geometry_msgs::PoseStamped pose_out;
    pose_out.header = lastPose->header;

    pose_out.pose.position.x = mocap_N;
    pose_out.pose.position.y = mocap_W;
    pose_out.pose.position.z = mocap_U;

    /*
    tf::Quaternion quat(lastPose->transform.rotation.y,
			    lastPose->transform.rotation.x,
			    lastPose->transform.rotation.z,
			    lastPose->transform.rotation.w);
    double roll, pitch, yaw;
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    tf::Matrix3x3 newMat;
    newMat.setRPY(-roll, pitch, yaw);
    tf::Quaternion newQuat;
    newMat.getRotation(newQuat);
    printf("X: %1.2f Y: %1.2f Z:%1.2f W:%1.2f\n", newQuat.x(), newQuat.y(), newQuat.z(), newQuat.w());
    */
    
    pose_out.pose.orientation.x = -lastPose->transform.rotation.y;
    pose_out.pose.orientation.y = lastPose->transform.rotation.x;
    pose_out.pose.orientation.z = lastPose->transform.rotation.z;
    pose_out.pose.orientation.w = lastPose->transform.rotation.w;
    pose_pub.publish(pose_out);

    //Next, the odometry:
    nav_msgs::Odometry odom_out;
    odom_out.header = lastPose->header;
    odom_out.pose.pose.position = pose_out.pose.position;
    odom_out.pose.pose.orientation.x = -lastPose->transform.rotation.y;
    odom_out.pose.pose.orientation.y = lastPose->transform.rotation.x;
    odom_out.pose.pose.orientation.z = lastPose->transform.rotation.z;
    odom_out.pose.pose.orientation.w = lastPose->transform.rotation.w;

    //Leaves covariances and twists (velocities) blank for now...
    odom_pub.publish(odom_out);
  }


  return 0;
}
